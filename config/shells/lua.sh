


if command -v luarocks >&/dev/null; then
  export LUAROCKS_CONFIG="$XDG_CONFIG_HOME/lua/luarocks.lua"
  export LUA_PATH="${BASHER_PREFIX}/var/lua/?.lua;${HOME}/opt/lua/?/init.lua;$( lua -e 'print(package.path)' )"
  export LUA_CPATH="${BASHER_PREFIX}/var/lua/?.so;${HOME}/opt/lua/loadall.so;$( lua -e 'print(package.cpath)' )"

  if [[ ! ":$PATH:" == *":${BASHER_PREFIX}/var/lua/bin:"* ]]; then
    export PATH="${BASHER_PREFIX}/var/lua/bin:$PATH"
  fi

fi

