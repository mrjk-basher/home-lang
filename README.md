
# Home Lang Management

This is a basher package to manage development tools.

## Quickstart

To install in production mode, just use basher:
```
basher install framagit.org/mrjk-basher/home-lang
```

## Development

Just install the package as a git repo:
```
basher install git@framagit.org:mrjk-basher/home-lang.git
```

## Details

Managed environment variables:
```
  COMPOSER_CACHE_DIR="${BASHER_PREFIX}/cache/composer"
  COMPOSER_HOME="${BASHER_PREFIX}/var/composer"
  GEM_HOME=${BASHER_PREFIX}/var/ruby
  GEMRC=${XDG_CONFIG_HOME}/ruby/gemrc
  GEM_SPEC_CACHE=${XDG_CACHE_HOME}/ruby/gem/specs
  GOPATH=${BASHER_PREFIX}/var/go
  LUA_CPATH="${BASHER_PREFIX}/var/lua/?.so;${HOME}/opt/lua/loadall.so;$( lua -e 'print(package.cpath)' )"
  LUA_PATH="${BASHER_PREFIX}/var/lua/?.lua;${HOME}/opt/lua/?/init.lua;$( lua -e 'print(package.path)' )"
  LUAROCKS_CONFIG="$XDG_CONFIG_HOME/lua/luarocks.lua"
  NPM_CONFIG_USERCONFIG=${XDG_CONFIG_HOME}/npmrc
  PYTHONUSERBASE=${BASHER_PREFIX}/var/python/home
  WORKON_HOME=${BASHER_PREFIX}/var/python/
```

## Credits

mrjk
