


export PYTHONUSERBASE=${BASHER_PREFIX}/var/python/home
if [[ ! ":$PATH:" == *":$PYTHONUSERBASE/bin:"* ]]; then
  export PATH="$PYTHONUSERBASE/bin:$PATH"
fi

if command -v virtualenvwrapper.sh >&/dev/null; then
  . virtualenvwrapper.sh
  if [ -z "$WORKON_HOME" ]; then
    export WORKON_HOME=${BASHER_PREFIX}/var/python/
  fi
fi

