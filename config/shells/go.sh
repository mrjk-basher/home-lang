

if command -v go >&/dev/null; then

  export GOPATH=${BASHER_PREFIX}/var/go

  if [[ ! ":$PATH:" == *":${GOPATH}/bin:"* ]]; then
    export PATH="${GOPATH}/bin:$PATH"
  fi

fi

